package examen3TT;

import java.time.LocalDate;
import java.util.ArrayList;

public class Gestoria implements EmpresaMatriz{

	private String nombre;
	private String cif;
	private String contacto;
	private double ingreMensual;
	private double gastMensual;
	ArrayList<Cliente> listaClientes;
	public Gestoria() {
		// TODO Auto-generated constructor stub
	}

	protected Gestoria(String nombre, String cif, String contacto, double ingreMensual, double gastMensual,ArrayList<Cliente> listaClientes) {
		super();
		this.nombre = nombre;
		this.cif = cif;
		this.contacto = contacto;
		this.ingreMensual = ingreMensual;
		this.gastMensual = gastMensual;
		this.listaClientes=new ArrayList<Cliente>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public double getIngreMensual() {
		return ingreMensual;
	}

	public void setIngreMensual(double ingreMensual) {
		this.ingreMensual = ingreMensual;
	}

	public double getGastMensual() {
		return gastMensual;
	}

	public void setGastMensual(double gastMensual) {
		this.gastMensual = gastMensual;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public int getCantidadClientes() {// devuelve la cantidad de citas de un medico
		return listaClientes.size();
	}
	
	public class Cliente{
		
		String nombreCompleto;
		String dni;
		int idCliente;
		LocalDate fechaEntrada;
		
		protected Cliente() {
			super();
		}

		protected Cliente(String nombreCompleto, String dni, int idCliente, LocalDate fechaEntrada) {
			super();
			this.nombreCompleto = nombreCompleto;
			this.dni = dni;
			this.idCliente = idCliente;
			this.fechaEntrada = fechaEntrada;
		}
		
		
	}

	@Override
	public double ingresosAnueles() {
		// TODO Auto-generated method stub
		return this.getIngreMensual()/12;
	}

	@Override
	public double gastosAnuales() {
		// TODO Auto-generated method stub
		return this.getGastMensual()/12;
	}
}
