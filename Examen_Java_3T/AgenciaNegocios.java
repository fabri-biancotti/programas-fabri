package examen3TT;

public class AgenciaNegocios implements EmpresaMatriz{

	private String razonSocial;
	private String cif;
	private String contacto;
	private int numEmpleados;
	private int numDepartamentos;
	private double ingresoMensual;
	private double gastosMensuales;
	
	public AgenciaNegocios() {
		// TODO Auto-generated constructor stub
	}

	protected AgenciaNegocios(String razonSocial, String cif, String contacto, int numEmpleados, int numDepartamentos,
			double ingresoMensual, double gastosMensuales) {
		super();
		this.razonSocial = razonSocial;
		this.cif = cif;
		this.contacto = contacto;
		this.numEmpleados = numEmpleados;
		this.numDepartamentos = numDepartamentos;
		this.ingresoMensual = ingresoMensual;
		this.gastosMensuales = gastosMensuales;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public int getNumEmpleados() {
		return numEmpleados;
	}

	public void setNumEmpleados(int numEmpleados) {
		this.numEmpleados = numEmpleados;
	}

	public int getNumDepartamentos() {
		return numDepartamentos;
	}

	public void setNumDepartamentos(int numDepartamentos) {
		this.numDepartamentos = numDepartamentos;
	}

	public double getIngresoMensual() {
		return ingresoMensual;
	}

	public void setIngresoMensual(double ingresoMensual) {
		this.ingresoMensual = ingresoMensual;
	}

	public double getGastosMensuales() {
		return gastosMensuales;
	}

	public void setGastosMensuales(double gastosMensuales) {
		this.gastosMensuales = gastosMensuales;
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("\nRazon Social: ");
		sb.append(razonSocial);
		sb.append("\nCIF: ");
		sb.append(cif);
		sb.append("\nContacto: ");
		sb.append(contacto);
		sb.append("\nNumero Empleados: ");
		sb.append(numEmpleados);
		sb.append("\nNumero Departamentos: ");
		sb.append(numDepartamentos);
		sb.append("\nIngresos Mensuales: ");
		sb.append(ingresoMensual);
		sb.append("\nGastos Mensuales: ");
		sb.append(gastosMensuales);
		return sb.toString();
	}

	@Override
	public double ingresosAnueles() {
		// TODO Auto-generated method stub
		
		return this.getIngresoMensual()/12;
	}

	@Override
	public double gastosAnuales() {
		// TODO Auto-generated method stub
		return this.getIngresoMensual()/12;
	} 
}
