package examen3TT;

public class Asesoria implements EmpresaMatriz{

	private String nombre;
	private String cif;
	private String contacto;
	private int numeroEmp;
	private double mensualIngreso;
	private double mensualGasto;
	
	public Asesoria() {
		// TODO Auto-generated constructor stub
	}

	protected Asesoria(String nombre, String cIF, String contacto, int numeroEmp, double mensualIngreso,
			double mensualGasto) {
		super();
		this.nombre = nombre;
		this.cif = cIF;
		this.contacto = contacto;
		this.numeroEmp = numeroEmp;
		this.mensualIngreso = mensualIngreso;
		this.mensualGasto = mensualGasto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCIF() {
		return cif;
	}

	public void setCIF(String cIF) {
		cif = cIF;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public int getNumeroEmp() {
		return numeroEmp;
	}

	public void setNumeroEmp(int numeroEmp) {
		this.numeroEmp = numeroEmp;
	}

	public double getMensualIngreso() {
		return mensualIngreso;
	}

	public void setMensualIngreso(double mensualIngreso) {
		this.mensualIngreso = mensualIngreso;
	}

	public double getMensualGasto() {
		return mensualGasto;
	}

	public void setMensualGasto(double mensualGasto) {
		this.mensualGasto = mensualGasto;
	}

	@Override
	public double ingresosAnueles() {
		// TODO Auto-generated method stub
		return this.getMensualIngreso()/12;
	}

	@Override
	public double gastosAnuales() {
		// TODO Auto-generated method stub
		return this.getMensualGasto()/12;
	}

}
