package examen3TT;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import examen3TT.Gestoria.Cliente;



public class InfoMatriz {

	public InfoMatriz() {
		// TODO Auto-generated constructor stub
	}

	public static ArrayList<Cliente> listaClientes(ArrayList<Gestoria>gestoria){
		ArrayList<Cliente> aux=new ArrayList<Cliente>();
		for(int i=0; i<gestoria.size(); i++) {
			if(!gestoria.get(i).getListaClientes().isEmpty()) {
				aux.addAll(gestoria.get(i).getListaClientes());
			}
		}
		Cliente client;
		for(int i=0; i<aux.size()-1; i++) {
			for(int j=0; j<aux.size()-i-1; j++) {
				if(aux.get(j+1).idCliente > aux.get(j).idCliente) {
					client =aux.get(j+1);
	 				aux.set(j+1,aux.get(j));
	 				aux.set(j,client);
					}
				}
			}
		/*Iterator<Gestoria>it=gestoria.iterator();
		while(it.hasNext()) {
			Gestoria gest=(Gestoria)it.next();	
			if(!gest.getListaClientes().isEmpty()) {
				aux.addAll(gest.getListaClientes());
			}
		}*/
		return aux;
	}
	
	public static double promedioEmpleados(ArrayList<Asesoria>asesoria) {
		double media=0;
		for(int i=0; i<asesoria.size(); i++) {
			media+=asesoria.get(i).getNumeroEmp();
		}
		return media/asesoria.size();
	}
	
	public static String[][] listaDepartamentos(ArrayList<AgenciaNegocios>agencia){
		
		String[][] result = new String[agencia.size()][2];
		int contador = 0;
		for (int i=0;i<agencia.size();i++){
			if (!agencia.isEmpty() && agencia.get(i).getIngresoMensual() > 5000){
				result[contador][0]=agencia.get(i).getRazonSocial();
				result[contador][1]= new Integer(agencia.get(i).getNumDepartamentos()).toString();
				contador++;
				}
			}
		return result;
	}
	
	public static ArrayList<Cliente> listaClientesAño(ArrayList<Gestoria>gestoria){
		//LocalDate fecha1=LocalDate.parse();
		Gestoria gestor=new Gestoria();
		ArrayList<Cliente> aux=new ArrayList<Cliente>();
		LocalDate hoy=LocalDate.now();   Cliente client=gestor.new Cliente("pepe","12345678",1,hoy);
		int anio=hoy.getYear();
		int anioCliente=client.fechaEntrada.getYear();
		for(int i=0; i<gestoria.get(i).getCantidadClientes(); i++) {
			if(anio == anioCliente) {
				aux.addAll(gestoria.get(i).getListaClientes());
				}
			}
		/*Iterator<Gestoria>it=gestoria.iterator();
		while(it.hasNext()) {
			Gestoria gest=(Gestoria)it.next();	
			if(!gest.getListaClientes().isEmpty() && anio == anioCliente) {
				aux.addAll(gest.getListaClientes());
			}
		}*/
		return null;
	}
	public static void main(String[]args) {
		ArrayList<Gestoria>listaGestor=new ArrayList<Gestoria>();
		ArrayList<Cliente>listaCliente=new ArrayList<Cliente>();
		LocalDate fecha1=LocalDate.of(2017,12,31);LocalDate fecha2=LocalDate.of(2019,07,22);LocalDate fecha3=LocalDate.of(2019,04,12);
		
		Gestoria gest1=new Gestoria("Gestor_1","abc123","abc@gmail.com",12000,9000,listaCliente);
		Cliente client1=gest1.new Cliente("juan","12345678",2,fecha1);    listaCliente.add(client1);
		Cliente client2=gest1.new Cliente("pedro","32165498",3,fecha2);    listaCliente.add(client2);
		
		ArrayList<Cliente>listaCliente2=new ArrayList<Cliente>();
		Gestoria gest2=new Gestoria("Gestoria Manola","123asd","asd@gmail.com",15000,4000,listaCliente2);
		Cliente client3=gest2.new Cliente("manolo","65432189",4,fecha3);    listaCliente2.add(client3);
		
		Gestoria gest3=new Gestoria("GestoresABC","987qwe","qwe@gmail.com",13500,3000,listaCliente2);
		Cliente client4=gest2.new Cliente("matias","65478932",5,fecha3);    listaCliente2.add(client4);
		
		listaGestor.add(gest3);listaGestor.add(gest2);listaGestor.add(gest1);
		listaClientes(listaGestor);
		//metodo 2º//
		ArrayList<Asesoria>listaAsesoria=new ArrayList<Asesoria>();
		//Asesoria ase1=new Asesoria("AsesorDC","123poi",);
	}
}
