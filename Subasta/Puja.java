package collections1;

import java.time.LocalDate;
import java.util.LinkedList;

public class Puja {

	private final Usuario usuario;
	private final double cantidad;
	
	public Puja() {
		LocalDate fecha=LocalDate.parse("2010-02-03");
		//LinkedList<Subasta> subastas=new LinkedList<Subasta>();
	 usuario=new Usuario("pepe",2420.00,"hola@gmail.com",fecha);	
	 this.cantidad=200.00;
	}
	public Puja(Usuario usuario,double ofrecerDinero) {
		// TODO Auto-generated constructor stub
		this.cantidad=ofrecerDinero;
		this.usuario=usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public double getCantidad() {
		return cantidad;
	}
	
}
