package collections1;

import java.time.LocalDate;
import java.util.LinkedList;

public class Usuario {

	//las constantes siempre deben estar inicializadas
	private final String nombre; // tambien puedo inicializar aca
	double credito;
	private String email;
	private LocalDate fechaNacimiento;
	private LinkedList<Subasta> subastas;
	
	public Usuario() {
		// TODO Auto-generated constructor stub
		this.nombre="pepe";
	}
	
	protected Usuario(String nombre, double credito, String email, LocalDate fechaNacimiento) {
		super();
		this.nombre = nombre;
		this.credito = credito;
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
		this.subastas=new LinkedList<Subasta>();
	}

	public void addSubasta(Subasta sub) {
		subastas.add(sub);
	}

	public String getNombre() {
		return nombre;
	}

	public double getCredito() {
		return credito;
	}

	public void setCredito(double credito) {
		this.credito = credito;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public void creditoIcre(double cant) {
		credito+=cant;
	}
	
	public void creditoDecre(double cant) {
	
		credito-=cant;
	}
}
