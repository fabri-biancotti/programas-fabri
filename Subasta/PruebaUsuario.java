package collections1;

import java.time.LocalDate;
import java.util.LinkedList;

public class PruebaUsuario {

	public PruebaUsuario() {
		// TODO Auto-generated constructor stub
	}

	public void main(String[] args) {
		
		Usuario Juan=new Usuario("Juan",100,"juan@gmail.com",LocalDate.of(1981, 12, 16));
		//
		Usuario Pedro=new Usuario("Pedro",150,"Pedro@gmail.com",LocalDate.of(1979, 6, 9));
		//
		Usuario Enrique=new Usuario("Enrique",300,"Enrique@gmail.com",LocalDate.of(1982, 3, 19));
		
		//Crea una subasta del producto "Tel�fono M�vil" cuyo propietario sea el usuario Juan.
		Subasta subasta = new Subasta(Juan,"Tel�fono M�vil");
		
		//El usuario Pedro puja por esa subasta 100 euros.
		subasta.pujar(Pedro,100);
		
		//Muestra en la consola la puja mayor de la subasta (nombre del usuario y cantidad).
		System.out.println(subasta.getPujaMayor());
		
		//El usuario Enrique puja por esa subasta 50 euros.
		subasta.pujar(Enrique,50);
		
		//Muestra en la consola la puja mayor. Comprueba que esta segunda puja no ha sido acepta, ya que es menor que la primera.
		System.out.println(subasta.getPujaMayor());
		
		//Ejecuta la subasta
		subasta.ejecutar();
		
		//El usuario Enrique puja de nuevo por esa subasta con 200 euros. Comprueba que no es aceptada, ya que la subasta ha sido cerrada.
		System.out.println(subasta.pujar(Enrique, 200));
		
		//Crea una subasta para el producto �Impresora l�ser� cuyo propietario es el usuario Pedro.
		Subasta subasta2 = new Subasta(Pedro,"Impresora Laser");
		
		//Enrique puja en esta nueva subasta por el m�nimo (1 euro m�s).
		subasta2.pujar(Enrique);
		
		//crea una colecci�n con los usuarios.
		LinkedList<Usuario> usuarios=new LinkedList<Usuario>();
		usuarios.add(Juan);
		usuarios.add(Pedro);
		usuarios.add(Enrique);
		
		//Muestra por la consola los cr�ditos de los tres usuarios.Los cr�ditos de Juan y Pedro han cambiado.
		for(Usuario usuario : usuarios) {
			System.out.println("Credito de "+ usuario.getNombre() + " = "+usuario.getCredito());
		}
	}
	
}
