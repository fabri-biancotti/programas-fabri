package collections1;

import java.util.LinkedList;

public class Subasta {

	private final String nombreProducto;
	private Usuario propietario;
	private boolean isOpen;
	private LinkedList<Puja> listaPujas;
	
	public Subasta() {
		// TODO Auto-generated constructor stub
		nombreProducto="pepe";
	}

	protected Subasta(String nombreProducto, Usuario propietario, boolean isOpen, LinkedList<Puja> listaPujas) {
		super();
		this.nombreProducto = nombreProducto;
		this.propietario = propietario;
		this.isOpen = true;
		this.listaPujas =new LinkedList<Puja>();
	
	}

	public Subasta(Usuario pro, String prod) {
		this.nombreProducto = prod;
		this.propietario = pro;
		this.listaPujas =new LinkedList<Puja>();
		this.propietario.addSubasta(this);
	}
	
	public String getNombreProducto() {
		return nombreProducto;
	}

	public Usuario getPropietario() {
		return propietario;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public LinkedList<Puja> getListaPujas() {
		return listaPujas;
	}

	public Puja getPujaMayor() {
		if(!listaPujas.isEmpty()) {
			return listaPujas.getLast();
		}else {
			return null;
		}
	}
	
	public boolean pujar(Usuario pujador,double cantidad){
		boolean puedoPujar=false;
		if(pujador != null && cantidad>0) {
			if(isOpen() == true && pujador.getCredito() >= cantidad && pujador != propietario && getPujaMayor().getCantidad() < cantidad) {
				Puja puja=new Puja(pujador,cantidad);
				listaPujas.add(puja);
				puedoPujar=true;
				}else {
					puedoPujar=false;
				}
			}else {
				puedoPujar=false;
			}
		return puedoPujar;
		}
	
	public boolean pujar(Usuario pujador) {
		double cant=1;
		if(getPujaMayor() != null) {
			cant+=getPujaMayor().getCantidad();
		}else {
			cant=1;
		}
		return pujar(pujador,cant);
	}
	
	public boolean ejecutar() {
	
		if(isOpen && getPujaMayor() != null) {
			Usuario ganador = getPujaMayor().getUsuario();
			double cantidad = getPujaMayor().getCantidad();
			propietario.creditoIcre(cantidad);
			ganador.creditoDecre(cantidad);
			isOpen=false;
			return true;
		}else {
			return false;
		}
		
	}
	@Override
	public String toString() {
		return getClass().getName() + " [producto=" + nombreProducto
									+ ", propietario=" + propietario.getNombre() 
									+ ", abierta=" + isOpen
									+ ", pujas=" + listaPujas.size() 
									+ "]";
	}
}
