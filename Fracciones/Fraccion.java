
public class Fraccion {
//Atributos
	int num;
	int den;
	
	public Fraccion() {
		this.num=1;
		this.den=2;
		
	}

	public Fraccion(int Num, int Den) {
		
		this.num = Num;
		
		if(den==0){
			this.den=1;
		}else {
			this.den=Den;
			simplificar();
		}
		
	}
	
	public Fraccion(int num) {
        this.num = num;
        this.den = 1;
    }

	
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getDen() {
		return den;
	}

	public void setDen(int den) {
		this.den = den;
	}
	
	public Fraccion Suma (Fraccion f) {
		Fraccion f1=new Fraccion();//se crea un objeto porque tiene que devolver una fraccion, para almacenar la suma
		f1.num=this.num*f.den+this.den*f.num;//multiplicacion en cruz
		f1.den=this.den*f.den;
		 f1.simplificar();
		return f1;
	}
	 //C�lculo del m�ximo com�n divisor por el algoritmo de Euclides
	  private int mcd() {
	        int u = Math.abs(num); //valor absoluto del numerador
	        int v = Math.abs(den); //valor absoluto del denominador
	        if (v == 0) {
	            return u;
	        }
	        int r;
	        while (v != 0) {
	            r = u % v;
	            u = v;
	            v = r;
	        }
	        return u;
	    }
	
	//metodo para simplificar
	private void simplificar() {
		 int n = mcd(); //se calcula el mcd de la fracci�n, para eso se llama al metodo
	        num = num / n;
	        den = den / n;
	}
	
	public Fraccion resta (Fraccion f) {
		Fraccion f1=new Fraccion();//se crea un objeto porque tiene que devolver una fraccion, para almacenar la suma
		f1.num=this.num*f.den-this.den*f.num;//multiplicacion en cruz
		f1.den=this.den*f.den;
		 f1.simplificar();
		return f1;
	}

	
}
