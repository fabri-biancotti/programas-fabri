<?php
session_start();
error_reporting(0);
$varsesion=$_SESSION['email'];

if($varsesion == null || $varsesion = ''){
  echo "<script>alert('Usted no tiene autorizacion')</script>";
  echo '<script>window.location="../index.html"</script>';
  die();
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ayuntamiento de Torrelobos</title>
	<link rel="shortcut icon" type="image/ico" href="../img/favicon.png"/>
      <link rel="stylesheet" type="text/css" href="../css/estilo.css">

  </head>
  <body>
    <header>
      <a><img src="../img/logosede.jpg" alt="/"></a>
      <table class="socialright">
          <tr class="none">
          <td class="none"><a href= "https://www.instagram.com"><img src="../img/ig.png" title="Instagram" alt="Instagram"></a></td>
          <td class="none"><a href= "https://www.twitter.com"><img src="../img/twitter.png" title="Twitter" alt="Twitter"></a></td>
          <td class="none"><a href= "https://www.youtube.com"><img src="../img/youtube.png" title="Youtube" alt="Youtube"></a></td>
          <td class="none"><a href= "https://www.facebook.com"><img src="../img/facebook.png" title="Facebook" alt="Facebook"></a></td>
        </tr>
      </table>
    </header>
	<nav class="navbar">
		  <a href="../index.html">Portada</a>
		  <div class="subnav">
			<button class="subnavbtn">Tu Ciudad <i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/calidadyambiente.html">Calidad y Medio Ambiente</a>
			  <a href="../html/transparencia.html">Transparencia</a>
			  <a href="../html/ayuntamiento.html">El Ayuntamiento</a>
        <a href="../html/comercio.html">Comercio</a>
			</div>
		  </div>
		  <div class="subnav">
			<button class="subnavbtn">Los Vecinos <i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/formaparte.html">Forma Parte</a>
			  <a href="../html/culturayocio.html">Cultura y Ocio</a>
			  <a href="../html/deportes.html">Deportes</a>
			  <a href="../html/empleo.html">Empleo</a>
			</div>
		  </div>
      <a href="../html/sede.html">Sede Electrónica</a>
		  <div class="subnav">
			<button class="subnavbtn">Servicios<i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/ofertapublica.html">Oferta Pública</a>
			  <a href="../html/serviciosempresariales.html">Servicio Empresariales</a>
			</div>
		  </div>
		  <a href="../html/contacto.html">Contacto</a>
	</nav>
<section class= "sectionpp">
  	<h2>Panel de administración.</h2>
  	<p>Bienvenido/a a la sede electrónica: <?php echo $_SESSION['email'] ?></p>
  </br></br></br>

  <section class= "centrada">
  <section class="sectionform">
    <h2><center>Cita previa</center></h2>
    <form  action="../php/cita.php" method="post">
      <p class="pform"><center><input type="text" name="nom" Placeholder="Nombre*" required/><center></p>
      <p class="pform"><center><input type="text"name="ape" Placeholder="Apellidos*" required/><center></p>
      <p class="pform"><center><input type= "text" name="dni" Placeholder="D.N.I." /><center></p>
      <p class="pform"><center><input type="text" name="mail" Placeholder="Email"/><center></p>
      <p class="pform"><center><input type="text" name="tel" Placeholder="Teléfono"/><center></p>
			<p class="pform"><center><select name="dep" required>
				<option disabled selected>Selecciona una opción*</option>
				<option value="atc">Atención al ciudadano</option>
				<option value="cul">Cultura</option>
				<option value="dep">Deprotes</option>
				<option value="eco">Medio ambiente</option>
				<option value="ecu">Economía</option>
				<option value="edu">Educación</option>
				<option value="emp">Empleo</option>
				<option value="fom">Fomento</option>
				<option value="hac">Hacienda</option>
				<option value="juv">Juventud</option>
				<option value="pol">Policía local</option>
				<option value="sal">Salud</option>
				<option value="sec">Secretaría</option>
				<option value="tic">Nuevas Tecnologías</option>
				<option value="urb">Urbanismo</option></select><center></p>
			<p class="pform"><center><input type="comment" name="mot"Placeholder="Motivo*" required/><center></p>
        <h5>*Obligatorio</h5>
      <p class="pform"><input type="submit" name="Enviar"/></p>
    </form>
  </section>
  <section class="sectionform">
    <h2><center>Formulario de pago</center></h2>
    <form action="../php/pago.php" method="post">
      <input type="text" name="emisora" Placeholder="Código del Pago (6 dígitos)*" required>
      <input type="text" name="concepto" Placeholder="Concepto*" required>
      <input type="text" name="referencia" Placeholder="Referencia*" required>
      <input type="text" name="identificacion" Placeholder="Identificación(10 dígitos)*" required>
      <input type="text" name="importe" Placeholder="Importe en €*" required>
      <h5><center>*Obligatorio</center></h5>
      <input type="submit" value="Aceptar">
    </form>
  </section>
  </section>
  <a href="empa.php"><img class= "jpgemp"  src="../img/emp.png" alt="/"/></a>
  </section>
  <section class="cerrar">
  <a href="../php/cerrar_session.php">Cerrar sesión</a>
</section>
</br></br></br>
<footer class="footer">
  <nav class="tr2">
  <h2>Enlaces externos de interés</h2>
    <a class="enlace" href="https://administracion.gob.es/" target="_blank">| Administración del Gobierno |</a>
    <a class="enlace" href="https://www.boe.es/" target="_blank">BOE |</a>
    <a class="enlace" href="https://transparencia.gob.es/" target="_blank">Transparencia del Gobierno |</a>
    <a class="enlace" href="https://europa.eu/youreurope/citizens/index_es.htm" target="_blank">Unión Europea |</a>
    <a class="enlace" href="https://administracionelectronica.gob.es/pae_Home#.XOvBXtMzau4" target="_blank">PAE |</a>
    <a class="enlace" href="https://www.lamoncloa.gob.es/Paginas/index.aspx" target="_blank">Moncloa |</a>
    <a class="enlace" href="https://www.agenda2030.gob.es/es" target="_blank">Agenda de Gobierno |</a>
  </nav>
  <h6>© 2019 Error 404. Todos los derechos reservados.</h6>
</footer>
</body>
</html>
