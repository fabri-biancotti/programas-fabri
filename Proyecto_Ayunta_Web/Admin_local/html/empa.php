<?php
session_start();
error_reporting(0);
$varsesion=$_SESSION['email'];

if($varsesion == null || $varsesion = ''){
  echo "<script>alert('Usted no tiene autorizacion')</script>";
  echo '<script>window.location="../index.html"</script>';
  die();
}
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ayuntamiento de Torrelobos</title>
	<link rel="shortcut icon" type="image/ico" href="../img/favicon.png"/>
      <link rel="stylesheet" type="text/css" href="../css/estilo.css">
  </head>
  <body>
    <header>
      <a><img src="../img/logofp.jpg" alt="/"></a>
      <table class="socialright">
          <tr class="none">
          <td class="none"><a href= "https://www.instagram.com"><img src="../img/ig.png" title="Instagram" alt="Instagram"></a></td>
          <td class="none"><a href= "https://www.twitter.com"><img src="../img/twitter.png" title="Twitter" alt="Twitter"></a></td>
          <td class="none"><a href= "https://www.youtube.com"><img src="../img/youtube.png" title="Youtube" alt="Youtube"></a></td>
          <td class="none"><a href= "https://www.facebook.com"><img src="../img/facebook.png" title="Facebook" alt="Facebook"></a></td>
        </tr>
      </table>
    </header>
	<nav class="navbar">
		  <a href="../index.html">Portada</a>
		  <div class="subnav">
			<button class="subnavbtn">Tu Ciudad <i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/calidadyambiente.html">Calidad y Medio Ambiente</a>
			  <a href="../html/transparencia.html">Transparencia</a>
			  <a href="../html/ayuntamiento.html">El Ayuntamiento</a>
        <a href="../html/comercio.html">Comercio</a>
			</div>
		  </div>
		  <div class="subnav">
			<button class="subnavbtn">Los Vecinos <i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/formaparte.html">Forma Parte</a>
			  <a href="../html/culturayocio.html">Cultura y Ocio</a>
			  <a href="../html/deportes.html">Deportes</a>
			  <a href="../html/empleo.html">Empleo</a>
			</div>
		  </div>
      <a href="../html/sede.html">Sede Electrónica</a>
		  <div class="subnav">
			<button class="subnavbtn">Servicios<i class="fa fa-caret-down"></i></button>
			<div class="subnav-content">
			  <a href="../html/ofertapublica.html">Oferta Pública</a>
			  <a href="../html/serviciosempresariales.html">Servicio Empresariales</a>
			</div>
		  </div>
		  <a href="../html/contacto.html">Contacto</a>
	</nav>
</br></br></br>
  <h1><center>Empadronate</center></h1>
  <section class="sectionform1">
  <form class="text1" action="../php/padron.php" method="post">
  <table>
  <tr>
  <td>Nombre*:<input type="text" name="nombre" Placeholder="Nombre" required></td>
  <td>Apellidos*:<input type="text" name="apellidos" Placeholder="Apellidos" required></td>
  <td>D.N.I.*:<input type="text" name="dni" Placeholder="D.N.I." required></td>
  <tr>
  <td>Dirección*:<input type="text" name="direccion" Placeholder="Dirección" required></td>
  <td>Fecha de Nacimiento*:<input type="date" name="fechaNacimiento" Placeholder="Fecha de Nacimiento" required></td>
  <td>Localidad*:<input type="text" name="localidad" Placeholder="Localidad"></td>
  <tr>
  <td>Email*:<input type="text" name="email" Placeholder="Correo Electrónico" required></td>
  <td>Telefono*:<input type="text" name="telefono" Placeholder="Telefono" required></td>
  <td>Codigo Postal*:<input type="text" name="postal" Placeholder="Codigo Postal" required></td>
  </table>
    <p>Tipo de Solicitud de Padrón Municipal de Habitantes*:</p>
    <p><input type="radio" name="consulta" value="otroMunicipio" > Alta procedente de otro municipio.</p>
    <p><input type="radio" name="consulta" value="cambioDomicilo" > Cambio de domicilio en el mismo municipio.</p>
    <p><input type="radio" name="consulta" value="nacimiento" > Nacimiento.</p>
    <p><input type="radio" name="consulta" value="omision" > Omisión.</p>
    <p><input type="radio" name="consulta" value="modificacion" > Modificación de datos personales.</p>
      <h5><center>*Obligatorio</center></h5>
    <p><input type="submit" value="Enviar">
  </form>
</section></br>
  <section class="cerrar">
  <a href="../php/cerrar_session.php">Cerrar sesión</a>
</section>
  </br></br>
  <footer class="footer">
    <nav class="tr2">
    <h2>Enlaces externos de interés</h2>
      <a class="enlace" href="https://administracion.gob.es/" target="_blank">| Administración del Gobierno |</a>
      <a class="enlace" href="https://www.boe.es/" target="_blank">BOE |</a>
      <a class="enlace" href="https://transparencia.gob.es/" target="_blank">Transparencia del Gobierno |</a>
      <a class="enlace" href="https://europa.eu/youreurope/citizens/index_es.htm" target="_blank">Unión Europea |</a>
      <a class="enlace" href="https://administracionelectronica.gob.es/pae_Home#.XOvBXtMzau4" target="_blank">PAE |</a>
      <a class="enlace" href="https://www.lamoncloa.gob.es/Paginas/index.aspx" target="_blank">Moncloa |</a>
      <a class="enlace" href="https://www.agenda2030.gob.es/es" target="_blank">Agenda de Gobierno |</a>
    </nav>
    <h6>© 2019 Error 404. Todos los derechos reservados.</h6>
  </footer>
  </body>
  </html>
